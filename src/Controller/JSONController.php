<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Entity\Sport;

class JSONController extends AbstractController
{
    /**
     * @Route("/json-sports", name="json-sports")
     */
    public function index()
    {

        $sport = $this->getDoctrine()
        ->getRepository(Sport::class)
        ->findAll();

        $serializer = $this->get('serializer');

        $response = $serializer->serialize($sport,'json');

        return new Response($response);

    }
}
