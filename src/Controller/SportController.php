<?php

namespace App\Controller;

use App\Entity\Sport;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;

class SportController extends AbstractController
{
    /**
     * @Route("/sports", name="sports")
     */
    public function index()
    {
        return $this->render('sport/index.html.twig', [
            'controller_name' => 'SportController',
        ]);
    }

    /**
        * @Route("/post-sports", name="post-sports", methods={"POST"}))
    */
    public function create(Request $request, EntityManagerInterface $em)
    {
        $request = $this->transformJsonBody($request);

        $sport = new Sport();
        
        $sport->setName($request->get('sportName'));
        $sport->setDescription($request->get('sportDescription'));
        $sport->setTeam($request->get('sportTeam'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($sport);
        $em->flush();

        return $this->handleView(
            $this->view(
                [
                    'status' => 'ok',
                ],
            )
        );
    }
}
