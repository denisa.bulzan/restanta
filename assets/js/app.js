/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require("../css/app.scss");

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require("jquery");
require("bootstrap");

import Vue from "vue";
import axios from "axios";
var jsapp = new Vue({
  el: "#js-app",
  delimiters: ["{*", "*}"],
  data: {
    message: "Hello World!",
    title: "Vue directive testing",
    sports: {},
    sportName: '',
    sportDescription: '',
    sportTeam: ''
  },
  methods: {
    submitData: function() {
      axios
        .post("http://127.0.0.1:8000/post-sports", this.data)
        .then(response => ('sport inserted'));
    }
  },
  mounted: function() {
    axios
      .get("http://127.0.0.1:8000/json-sports")
      .then(response => (this.sports = response));
  }
});
